# Django Sample Application

This repository provides a sample Python web application implemented using the Django web framework and hosted using the Django development server. It is intended to be used to demonstrate deployment of Python web applications to OpenShift 3.

## Production Warnings

The manner in which this sample application has been set up is not suitable for a production deployment. For a production deployment the following changes would be required.

* A production grade WSGI server such as ``mod_wsgi`` or ``gunicorn`` should be used instead of the Django development server.
* The Django ``DEBUG`` setting should not be set to ``True`` as any exception in the application code can reveal sensitive details to a user.
* The Django ``SECRET_KEY`` should not be set in the application code and instead should be set via an environment variable in the application deployment configuration for OpenShift.
* A persistent volume would need to be used to store the SQLite database, and preferably a separate database service such as PostgreSQL or MySQL used instead.
* Automatic database migration support as part of application deployment, provided by the default S2I builder for Python, should be disabled.

For sample Django applications more suitable for deployment in a production environment see:

* https://gitlab.com/osevg/python-django-modwsgi
* https://gitlab.com/osevg/python-django-gunicorn

## Implementation Notes

This sample Python application relies on the support provided by the default S2I builder for deploying a Django application using the Django development server. The requirements which need to be satisfied for this to work are:

* The ``gunicorn`` package must **NOT** be listed in the ``requirements.txt`` file for ``pip``.

## Deployment Steps

To deploy this sample Python web application from the OpenShift web console, you should select ``python:2.7``, ``python:3.4`` or ``python:latest``, when using _Add to project_. Use of ``python:latest`` is the same as having selected the most up to date Python version available, which at this time is ``python:3.4``.

The HTTPS URL of this code repository which should be supplied to the _Git Repository URL_ field when using _Add to project_ is:

* https://gitlab.com/osevg/python-django-runserver.git

If using the ``oc`` command line tool instead of the OpenShift web console, to deploy this sample Python web application, you can run:

```
oc new-app https://gitlab.com/osevg/python-django-runserver.git
```

In this case, because no language type was specified, OpenShift will determine the language by inspecting the code repository. Because the code repository contains a ``requirements.txt``, it will subsequently be interpreted as including a Python application. When such automatic detection is used, ``python:latest`` will be used.

If needing to select a specific Python version when using ``oc new-app``, you should instead use the form:

```
oc new-app python:2.7~https://gitlab.com/osevg/python-django-runserver.git
```

For this sample application the database will be created upon the first deployment, with database migrations being automatically performed on subsequent deployments if changes were made to the database model. This capability should only be relied upon in a development environment and should be disabled for a production deployment.

Although the database will be initialised, no super user account will be created. To create this you will need to access the running container for the application using an interactive shell and manually run:

```
python manage.py createsuperuser
```

You will then be able to login into the Django admin interface for the application.
